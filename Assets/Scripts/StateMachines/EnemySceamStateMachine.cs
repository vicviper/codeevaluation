﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySceamStateMachine : EnemyStateMachineBase
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        bp.status = AIStatus.WaitToDone;
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (bp != null && bp.status == AIStatus.Die) return;
        if (bp != null && bp.status == AIStatus.WaitToDone)
        {
            bp.status = AIStatus.Chase;
            bp.ChaseTarget();
        }
    }

}
