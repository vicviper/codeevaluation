﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovingStateMachine : EnemyStateMachineBase
{
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (bp != null && bp.status == AIStatus.Die) return;
        if (bp != null && (bp.status == AIStatus.Chase || bp.status == AIStatus.Chasing))
        {
            UpdateDestination();
            CheckReached();
        }

        if (bp != null && bp.status == AIStatus.WaitToPatrol)
        {
            CheckReachedWayPoint();
        }
    }
}
