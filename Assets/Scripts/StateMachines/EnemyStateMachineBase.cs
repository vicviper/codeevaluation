﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachineBase : StateMachineBehaviour
{
    protected BehaviourEnemy bp;
    protected Animator ani;
    protected float actionCooltime = 0f;
    protected float lastActionTime = 0f;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetComponent<BehaviourEnemy>() != null)
        {
            bp = animator.GetComponent<BehaviourEnemy>();
            actionCooltime = bp.characterEntity.actionCooltime;
            ani = animator;
        }
        else
        {
            return;
        }
    }

    protected void UpdateDestination()
    {
        if (bp.targetObj == null) return;
        bp.navAgent.SetDestination(bp.targetObj.transform.position);
    }

    protected void CheckReachedWayPoint()
    {
        if (bp.navAgent.stoppingDistance < Vector3.Distance(bp.transform.position, bp.navAgent.destination)) return;
        bp.status = AIStatus.ReachedWayPoint;
        ani.SetTrigger("Idle");
        bp.waypointReachedTime = Time.time;
    }

    protected void CheckReached()
    {
        if (bp.navAgent.stoppingDistance < Vector3.Distance(bp.transform.position, bp.navAgent.destination)) return;

        if (bp.targetObj == null)
        {
            bp.ChangeStatus(AIStatus.Idle);
        }
        else if (bp.targetObj != null)
        {
            bp.ChangeStatus(AIStatus.Action);
        }
    }
}