﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachineBase : StateMachineBehaviour
{
    protected BehaviourPlayer bp;
    protected float actionCooltime = 0f;
    protected float lastActionTime = 0f;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetComponent<BehaviourPlayer>() != null)
        {
            bp = animator.GetComponent<BehaviourPlayer>();
            actionCooltime = bp.characterEntity.actionCooltime;
        }
        else
        {
            return;
        }

    }

    protected void UpdateDestination()
    {
        if (bp.targetObj == null) return;
        bp.navAgent.SetDestination(bp.targetObj.transform.position);
    }

    protected void CheckReached()
    {
        if (bp.navAgent.stoppingDistance < Vector3.Distance(bp.transform.position, bp.navAgent.destination)) return;

        if (bp.targetObj == null)
        {
            bp.ChangeStatus(AIStatus.Idle);
        }
        else if (bp.targetObj != null)
        {
            Vector3 direction = (bp.targetObj.transform.position - bp.transform.position).normalized;
            bp.transform.forward = direction;
            bp.ChangeStatus(AIStatus.Action);
        }
    }
}
