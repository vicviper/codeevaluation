﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovingStateMachine : PlayerStateMachineBase
{
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (bp != null && bp.status == AIStatus.Chase)
        {
            UpdateDestination();
        }

        if (bp != null && (bp.status == AIStatus.Walk || bp.status == AIStatus.Run || bp.status == AIStatus.Chase))
        {
            CheckReached();
        }
    }
}
