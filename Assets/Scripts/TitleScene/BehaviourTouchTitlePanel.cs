﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTouchTitlePanel : UIEventManager
{
    override public void OKEvent()
    {
        TitleManager.instance.LoadNextScene();
    }
}
