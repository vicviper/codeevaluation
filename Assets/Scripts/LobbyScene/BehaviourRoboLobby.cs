﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourRoboLobby : MonoBehaviour
{
    const string ANIMATOR_TRIGGER_NO = "No";
    const string ANIMATOR_TRIGGER_YES = "Yes";
    public Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void SaySomthing(int idx)
    {
        if (animator == null) return;
        animator.SetTrigger( (idx == 1) ? ANIMATOR_TRIGGER_YES : ANIMATOR_TRIGGER_NO);
    }
}
