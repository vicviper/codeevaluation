﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourButtonLv : UIEventManager
{
    public int lvIdx = 0;

    override public void OKEvent()
    {
        LobbyMananger.instance.robo.SaySomthing(lvIdx);
        if (lvIdx == 1)
        {
            LobbyMananger.instance.LoadNextScene();
        }
    }
}
