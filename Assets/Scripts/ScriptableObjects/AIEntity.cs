﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum AIStatus { Init, Idle, Walk, Run, Patrol, WaitToPatrol, ReachedWayPoint, Found, Chase, Chasing, Action, WaitToDone, Die };
public enum ActionType { MeleeAttack, Shoot };

[CreateAssetMenu(fileName = "New Character", menuName = "Make a new Character")]

public class AIEntity : ScriptableObject
{
    [Header("Basic")]
    public string botName;
    public bool controllable = true;

    [Header("Status")]
    public AIStatus status = AIStatus.Init;

    [Header("Move")]
    [Range(0f, 10f)]
    public float walkSpeed = 1f;
    [Range(0f, 10f)]
    public float chaseSpeed = 2f;
    [Range(0f, 5f)]
    public float patrolCoolTime = 3f;

    [Header("Health")]
    [Range(100f, 1000f)]
    public float HP = 100f;

    [Header("Target Sensor(NPC assign only)")]
    [Range(1f, 5f)]
    public float sensorRadius = 2.5f;
    public float sensorCoolTime = 1f;

    [Header("Actions")]
    [Range(1f, 50f)]
    public float actionDistance = 1f;
    public ActionType actionType = ActionType.MeleeAttack;
    public float actionAmount = 30f;
    [Range(0.5f, 10f)]
    public float actionCooltime = 1f;
    [Range(0.0f, 1f)]
    public float criticalRateOnFront = 0.1f;
    [Range(0.0f, 1f)]
    public float criticalRateOnSide = 0.2f;
    [Range(0.0f, 1f)]
    public float criticalRateOnBack = 1.0f;
    [Range(0.0f, 10f)]
    public float criticalAmount = 1.5f;

}
