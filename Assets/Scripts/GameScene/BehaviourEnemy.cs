﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourEnemy : CharacterBase
{
    [SerializeField]
    public Transform[] waypoints;
    int waypointNum = -1;
    float sensingLastTime = 0f;
    public float waypointReachedTime = 0f;
    float actionLastTime = 0f;
    

    void Start()
    {
        InitEntity();
        ChangeStatus(AIStatus.Patrol);
    }

    void Update()
    {
        if (status == AIStatus.Die) return;

        if (status == AIStatus.Patrol)
        {
            DoNextPatrol();
        }

        if (status == AIStatus.ReachedWayPoint && waypointReachedTime + characterEntity.patrolCoolTime < Time.time)
        {
            ChangeStatus(AIStatus.Patrol);
        }

        if ((status == AIStatus.WaitToPatrol || status == AIStatus.Patrol || status == AIStatus.ReachedWayPoint || status == AIStatus.Idle) &&
            sensingLastTime + characterEntity.sensorCoolTime < Time.time)
        {
            sensingLastTime = Time.time;
            TargetInSight();
        }

        if (status == AIStatus.Action && (actionLastTime + characterEntity.actionCooltime < Time.time))
        {
            actionLastTime = Time.time;
            DoAction();
        }

        if (status == AIStatus.Chase)
        {
            ChaseTarget();
        }
    }

    void DoNextPatrol()
    {
        ChangeStatus(AIStatus.WaitToPatrol);
        waypointNum++;
        if (waypointNum >= waypoints.Length)
        {
            waypointNum = 0;
        }
        navAgent.speed = characterEntity.walkSpeed;
        navAgent.SetDestination(waypoints[waypointNum].position);
        navAgent.stoppingDistance = NAV_STOP_DISTANCE;
        ani.SetTrigger(ANI_WALK);
    }

    void TargetInSight()
    {
        if (targetObj == null || GetDirectionAngle(true) != DirectionToObject.Forward) return;

        Ray r = new Ray(transform.position + Vector3.up, (targetObj.position - transform.position));
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, Mathf.Infinity))
        {
            if (hit.transform.tag == PLAYER_TAG)
            {
                ChangeStatus(AIStatus.Found);
                ani.SetTrigger(ANI_DETECT);
            }
        }
    }

    public void DoAction()
    {
        if (status == AIStatus.Die) return;
        if (characterEntity.actionType == ActionType.MeleeAttack)
        {
            ani.SetTrigger(ANI_ACTION_MELEE);
            //Will trigger AttackToTarget after playing animation on PlayerActionStateMachine.cs
        }
    }

    public void ChaseTarget()
    {
        if (characterEntity == null || targetObj == null || transform == null) return;

        if (characterEntity.actionDistance < Vector3.Distance(targetObj.position, transform.position))
        {
            status = AIStatus.Chasing;
            navAgent.speed = characterEntity.chaseSpeed;
            navAgent.SetDestination(targetObj.position);
            navAgent.stoppingDistance = characterEntity.actionDistance;
            ani.SetTrigger(ANI_RUN);

        } else
        {
            status = AIStatus.Action;
        }
        
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag != PLAYER_TAG) return;
        targetObj = other.transform;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag != PLAYER_TAG) return;
        targetObj = null;
        ChangeStatus(AIStatus.Patrol);
    }
}
