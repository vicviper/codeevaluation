﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXDisabler : MonoBehaviour
{
    void OnEnable()
    {
        if (GetComponent<ParticleSystem>() == null) return;
        ParticleSystem ps = GetComponent<ParticleSystem>();
        StartCoroutine(TimeOut(ps.main.duration + ps.main.startLifetimeMultiplier));
    }

    IEnumerator TimeOut(float lifeTime)
    {
        yield return new WaitForSeconds(lifeTime);
        gameObject.SetActive(false);
    }
}
