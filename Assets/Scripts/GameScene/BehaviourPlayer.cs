﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
public class BehaviourPlayer : CharacterBase
{
    enum ClickEventResult { None, Single, Double };

    const float DOUBLE_CLICK_DELTA_TIME = 0.3f;
    const string MOUSE_LEFT_KEY_NAME = "Fire1";

    ClickEventResult clickEvent = ClickEventResult.None;
    int clicked = 0;
    float clickLastTime = 0f;
    Vector3 clickPosition = Vector3.zero;
    float actionLastTime = 0f;
    

    void Start()
    {
        InitEntity();
    }

    /*
     Walk & Run, Chase needs UpdateDestination() & CheckReached(). 
     - moved these method into PlayerStateMachineBase.cs & PlayerMovingStateMachine to control directly on Animator and to simplify Update().
     We need a transition after playing Action animation to idle.
     - moved related method into PlayerActionStateMachine.cs
    */
    void Update()
    {
        GetMiceEvent();

        if (status == AIStatus.Action && (actionLastTime + characterEntity.actionCooltime < Time.time))
        {
            actionLastTime = Time.time;
            DoAction();
        }

        SetAnimationIdle();
    }

    void DoAction()
    {
        if (characterEntity.actionType == ActionType.MeleeAttack)
        {
            ani.SetTrigger(ANI_ACTION);
            //Will trigger AttackToTarget after playing animation on PlayerActionStateMachine.cs
        }
    }

    #region below all method have related mouse click
    void GetMiceEvent()
    {
        clickEvent = CheckInput();
        if (clickEvent != ClickEventResult.None)
        {
            ActionDecide((clickEvent == ClickEventResult.Single) ? true : false);
        }
    }

    ClickEventResult CheckInput()
    {
        if (clicked == 1 && (Time.time - clickLastTime) > DOUBLE_CLICK_DELTA_TIME)
        {
            clicked = 0;
            clickLastTime = 0f;
            return ClickEventResult.Single;
        }

        if (Input.GetButtonDown(MOUSE_LEFT_KEY_NAME))
        {
            clicked++;
            clickLastTime = Time.time;
            clickPosition = Input.mousePosition;
            if (clicked == 2 && (Time.time - clickLastTime) <= DOUBLE_CLICK_DELTA_TIME)
            {
                clicked = 0;
                clickLastTime = 0f;
                return ClickEventResult.Double;
            }
        }
        return ClickEventResult.None;
    }

    void ActionDecide(bool singleClick)
    {
        Ray r = Camera.main.ScreenPointToRay(clickPosition);
        RaycastHit hit;

        if (Physics.Raycast(r, out hit, Mathf.Infinity) && (IsReachable(transform.position, hit.point)))
        {
            navAgent.SetDestination(hit.point);
            navAgent.speed = (singleClick) ? characterEntity.walkSpeed : characterEntity.chaseSpeed;
            ani.SetTrigger((singleClick) ? ANI_WALK : ANI_RUN);

            if (hit.transform.tag != ENEMY_TAG)
            {
                targetObj = null;
                navAgent.stoppingDistance = NAV_STOP_DISTANCE;
                status = (singleClick) ? AIStatus.Walk : AIStatus.Run;
            }
            else if (hit.transform.tag == ENEMY_TAG)
            {
                targetObj = hit.transform;
                navAgent.stoppingDistance = characterEntity.actionDistance;
                status = AIStatus.Chase;
            }
        }
    }
    #endregion
}
