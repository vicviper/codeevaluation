﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourFood : MonoBehaviour
{
    const string PLAYER_TAG = "Player";
    
    const float MULTIPLIES_FACTOR = 1f;
    const float ROT_TIME_SPEED = 5f;
    const float HEIGHT_FACTOR = 0.8f;
    const float MOVE_TIME_SPEED = 1f;

    readonly iTween.LoopType ROT_LOOP_TYPE = iTween.LoopType.loop;
    readonly iTween.EaseType EASE_TYPE = iTween.EaseType.linear;
    readonly iTween.LoopType MOV_LOOP_TYPE = iTween.LoopType.pingPong;

    public int score = 100;

    MeshRenderer meshRenderer;
    SphereCollider col;

    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        col = GetComponent<SphereCollider>();
        DoRotateUpDown();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != PLAYER_TAG) return;
        meshRenderer.enabled = false;
        col.enabled = false;
        GameManager.instance.GetFood(score, transform.position);
        Destroy(gameObject);
    }

    void DoRotateUpDown()
    {
        iTween.RotateBy(gameObject, iTween.Hash(
            "y", MULTIPLIES_FACTOR,
            "time", ROT_TIME_SPEED,
            "looptype", ROT_LOOP_TYPE,
            "easetype", EASE_TYPE
            )
        );

        iTween.MoveFrom(gameObject, iTween.Hash(
            "y", HEIGHT_FACTOR,
            "time", MOVE_TIME_SPEED,
            "looptype", MOV_LOOP_TYPE,
            "easetype", EASE_TYPE
            )
        );
    }
}
