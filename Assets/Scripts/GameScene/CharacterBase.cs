﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterBase : MonoBehaviour
{
    protected enum DirectionToObject { Forward, Side, Back };

    const float ANGLE_FORWARD_LIMIT = 45f;
    const float ANGLE_BACK_LIMIT = 135f;
    const float DEATH_TIME_OFFSET = 3f;
    protected const float NAV_STOP_DISTANCE = 0.1f;
    protected const string SKIP_TAG = "HiddenPoint";
    protected const string ENEMY_TAG = "Enemy";
    protected const string PLAYER_TAG = "Player";
    protected const string ANI_IDLE = "Idle";
    protected const string ANI_WALK = "Walk";
    protected const string ANI_RUN = "Run";
    protected const string ANI_ACTION = "Punch";
    protected const string ANI_ACTION_MELEE = "Attack";
    protected const string ANI_DIZZY = "Dizzy";
    protected const string ANI_DIE = "Die";
    protected const string ANI_DETECT = "Detect";
    protected const string ANI_CHASE = "Chase";

    public AIEntity characterEntity;
    public NavMeshAgent navAgent;
    public Transform targetObj;

    protected AIStatus previousStatus = AIStatus.Init;
    protected Rigidbody rb;
    protected Animator ani;

    public AIStatus status = AIStatus.Init;

    public float HP;

    protected void InitEntity()
    {
        navAgent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
        ani = GetComponent<Animator>();
        HP = characterEntity.HP;
        ChangeStatus(AIStatus.Idle);
        if (characterEntity.controllable == true) return;
        GetComponent<SphereCollider>().radius = characterEntity.sensorRadius;
        status = characterEntity.status;
    }

    protected void SetAnimationIdle()
    {
        if (status != previousStatus && status == AIStatus.Idle)
        {
            previousStatus = status;
            ani.SetTrigger(ANI_IDLE);
        }
    }

    public void ChangeStatus(AIStatus currentStatus)
    {
        previousStatus = status;
        status = currentStatus;
    }

    public bool IsReachable(Vector3 startPoint, Vector3 goalPoint)
    {
        bool result = false;
        NavMeshPath path = new NavMeshPath();
        if (NavMesh.CalculatePath(startPoint, goalPoint, NavMesh.AllAreas, path))
        {
            return true;
        }
        return result;
    }

    //Trigger by PlayerActionStateMachine.cs
    //Trigger by EnemyActionStateMachine.cs
    public void AttackToTarget()
    {
        float damage = 0f;
        GameManager.instance.ToDamage(targetObj.position);
        if (GetDirectionAngle() == DirectionToObject.Forward)
        {
            damage = (Random.Range(0f, 1f) < characterEntity.criticalRateOnFront) ? characterEntity.criticalAmount * characterEntity.actionAmount : characterEntity.actionAmount;
        }
        if (GetDirectionAngle() == DirectionToObject.Side)
        {
            damage = (Random.Range(0f, 1f) < characterEntity.criticalRateOnSide) ? characterEntity.criticalAmount * characterEntity.actionAmount : characterEntity.actionAmount;
        }
        if (GetDirectionAngle() == DirectionToObject.Back)
        {
            damage = (Random.Range(0f, 1f) < characterEntity.criticalRateOnBack) ? characterEntity.criticalAmount * characterEntity.actionAmount : characterEntity.actionAmount;
        }
        
        if (targetObj.GetComponent<CharacterBase>() != null)
        {
            targetObj.GetComponent<CharacterBase>().DamagaTarget(damage);
        }
    }

    public void DamagaTarget(float damage)
    {
        HP -= damage;
        if (HP > 0 && transform.tag == ENEMY_TAG && status != AIStatus.Found)
        {
            ChangeStatus(AIStatus.Found);
            ani.SetTrigger(ANI_DETECT);
        }

        if (HP <= 0 && status != AIStatus.Die)
        {
            ChangeStatus(AIStatus.Die);
            ani.SetTrigger(ANI_DIE);
            if (transform.tag == ENEMY_TAG)
            {
                float deathTime = ani.GetCurrentAnimatorClipInfo(0)[0].clip.length * ani.GetCurrentAnimatorStateInfo(0).normalizedTime;
                Destroy(gameObject, deathTime + DEATH_TIME_OFFSET);
            } else
            {
                GameManager.instance.FailGame();
            }
        }
    }

    protected DirectionToObject GetDirectionAngle(bool beSearching = false)
    {
        Vector3 directionFromTarget = (!beSearching) ? (transform.position - targetObj.position) : (targetObj.position - transform.position);
        float approachedAngle = (Vector3.SignedAngle(directionFromTarget, (!beSearching) ? targetObj.forward : transform.forward, Vector3.up));

        if (Mathf.Abs(approachedAngle) < ANGLE_FORWARD_LIMIT)
        {
            return DirectionToObject.Forward;
        }
        else if (Mathf.Abs(approachedAngle) > ANGLE_BACK_LIMIT)
        {
            return DirectionToObject.Back;
        }
        return DirectionToObject.Side;
    }
}
