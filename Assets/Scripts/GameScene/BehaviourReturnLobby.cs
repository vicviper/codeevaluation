﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BehaviourReturnLobby : MonoBehaviour
{
    const string PLAYER_TAG = "Player";

    public Image panelReturn;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == PLAYER_TAG)
        {
            panelReturn.gameObject.SetActive(true);
            GameManager.instance.SetPause();
        }
    }
}
