﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BehaviourGoal : MonoBehaviour
{
    const string PLAYER_TAG = "Player";
    public Image winPanel;

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == PLAYER_TAG)
        {
            winPanel.gameObject.SetActive(true);
            GameManager.instance.SetPause();
        }
    }
}
