﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourDeathPanel : UIEventManager
{
    override public void OKEvent()
    {
        GameManager.instance.ReloadScene();
    }
}
