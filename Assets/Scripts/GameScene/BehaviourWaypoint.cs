﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourWaypoint : MonoBehaviour
{
    public enum GizmoColor { red, blue, yellow, green };
    public GizmoColor color = GizmoColor.red;

    void OnDrawGizmos()
    {
        
        Gizmos.color = Color.white;

        if (color == GizmoColor.red)
            Gizmos.color = Color.red;
        if (color == GizmoColor.blue)
            Gizmos.color = Color.blue;
        if (color == GizmoColor.yellow)
            Gizmos.color = Color.yellow;
        if (color == GizmoColor.green)
            Gizmos.color = Color.green;

        Gizmos.DrawSphere(transform.position, 0.5f);
    }
}
