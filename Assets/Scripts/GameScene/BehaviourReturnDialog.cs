﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourReturnDialog : UIEventManager
{
    const string BUTTON_YES = "ButtonYes";
    const string BUTTON_NO = "ButtonNo";

    public GameObject panelRoot;

    override public void OKEvent()
    {
        if (transform.name == BUTTON_NO)
        {
            GameManager.instance.SetResume();
            panelRoot.SetActive(false);
        }

        if (transform.name == BUTTON_YES)
        {
            GameManager.instance.LoadLobbyScene();
        }
    }
}

