﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneAndFadeManager : MonoBehaviour
{
    protected const float FADEOUT_ALPHA = 0f;
    protected const float FADEIN_ALPHA = 1f;
    protected const float FADETIME = 1.5f;

    public Image screenBlocker;

    protected void FadeoutScreenBlocker()
    {
        if (screenBlocker == null) return;

        screenBlocker.gameObject.SetActive(true);
        iTween.ColorTo(screenBlocker.gameObject,
            iTween.Hash(
                "a", FADEOUT_ALPHA,
                "time", FADETIME,
                "oncomplete", (System.Action<object>)(x => screenBlocker.gameObject.SetActive(false))
            )
        );
    }

    protected void LoadSceneEffect(string sceneName)
    {
        if (screenBlocker == null) return;

        screenBlocker.gameObject.SetActive(true);
        iTween.ColorTo(screenBlocker.gameObject,
            iTween.Hash(
                "a", FADEIN_ALPHA,
                "time", FADETIME,
                "oncomplete", (System.Action<object>)(x => StartCoroutine(LoadAsyncScene(sceneName)))
            )
        );
    }

    protected IEnumerator LoadAsyncScene(string nextScene)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(nextScene);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
