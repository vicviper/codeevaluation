﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIEventManager : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler, IPointerClickHandler
{
    const string EXCEPT_OBJECT_NAME = "PanelBasic";
    const string LARGE_ANI_NAME = "larger";
    const string DOWN_ANI_NAME = "downer";
    const float ORIGIN_SIZE_FACTOR = 1.0f;
    const float LARGE_SIZE_FACTOR = 1.1f;
    const float LARGE_ANI_TIME = 0.5f;
    const float DOWN_OFFSET_Y = -3f;

    Vector3 originPosition = Vector3.zero;
    GameObject selectedObject;

    public void OnPointerEnter(PointerEventData eventData)
    {
        originPosition = transform.localPosition;

        if (EXCEPT_OBJECT_NAME == gameObject.name) return;
        CancelAnimation();
        iTween.ScaleTo(gameObject, iTween.Hash(
                "name", LARGE_ANI_NAME,
                "x", LARGE_SIZE_FACTOR,
                "y", LARGE_SIZE_FACTOR,
                "time", LARGE_ANI_TIME,
                "ignoretimescale", true
            )
         );
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        selectedObject = gameObject;

        if (EXCEPT_OBJECT_NAME == gameObject.name) return;
        CancelAnimation();
        iTween.MoveTo(gameObject, iTween.Hash(
                "y", originPosition.y + DOWN_OFFSET_Y,
                "islocal", true,
                "time", LARGE_ANI_TIME,
                "ignoretimescale", true
            )
         );

        PressedEvent();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        selectedObject = null;

        if (EXCEPT_OBJECT_NAME == gameObject.name) return;
        CancelAnimation();
        iTween.MoveTo(gameObject, iTween.Hash(
                "y", originPosition.y,
                "islocal", true,
                "time", LARGE_ANI_TIME,
                "ignoretimescale", true
            )
        );

        iTween.ScaleTo(gameObject, iTween.Hash(
                "name", LARGE_ANI_NAME,
                "x", ORIGIN_SIZE_FACTOR,
                "y", ORIGIN_SIZE_FACTOR,
                "time", LARGE_ANI_TIME,
                "ignoretimescale", true
            )
         );
        CancelEvent();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (selectedObject != gameObject)
        {
            CancelEvent();
            return;
        }

        selectedObject = null;
        OKEvent();

        if (EXCEPT_OBJECT_NAME == gameObject.name) return;
        iTween.MoveTo(gameObject, iTween.Hash(
                "y", originPosition.y,
                "islocal", true,
                "time", LARGE_ANI_TIME,
                "ignoretimescale", true
            )
        );

        
    }

    void CancelAnimation()
    {
        iTween.StopByName(gameObject, LARGE_ANI_NAME);
        iTween.StopByName(gameObject, DOWN_ANI_NAME);
    }

    public virtual void PressedEvent() { }
    public virtual void CancelEvent() { }
    public virtual void OKEvent() { }
}
