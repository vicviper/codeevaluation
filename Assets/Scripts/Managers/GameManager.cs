﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using TMPro;


public class GameManager : SceneAndFadeManager
{
    const float DEATH_FADEIN_ALPHA = 0.5f;
    const float DEATH_FADETIME = 3f;
    const string GAME_SCENE_NAME = "GameScene";
    const string LOBBY_SCENE_NAME = "LobbyScene";
    const string PLAYER_ADDRESS = "Robo";
    const string EXPLOSION_FX_ADDRESS = "FoodExplosion";
    const string HIT_FX_ADDRESS = "HitYellow";
    const float AUTO_DESTRUCTION_TIME = 3f;

    public static GameManager instance;
    public TextMeshProUGUI txtScore;

    public Vector3 playerInitPosition;

    public int score;
    public Image panelDead;

    void Awake()
    {
        instance = this;
    }

    public void GetFood(int foodScore, Vector3 foodPosition)
    {
        score += foodScore;
        txtScore.text = score.ToString();
        PoolManager.instance.Spawn(EXPLOSION_FX_ADDRESS, foodPosition, Quaternion.identity);
        iTween.ScaleFrom(txtScore.gameObject, Vector3.one * 1.2f, 1f);
    }

    public void ToDamage(Vector3 postion)
    {
        PoolManager.instance.Spawn(HIT_FX_ADDRESS, postion + Vector3.up, Quaternion.identity);
    }

    public void SetPause()
    {
        Time.timeScale = 0f;
    }

    public void SetResume()
    {
        Time.timeScale = 1f;
    }

    public void FailGame()
    {
        if (panelDead == null) return;
        panelDead.gameObject.SetActive(true);
        iTween.ColorTo(panelDead.gameObject,
            iTween.Hash(
                "a", DEATH_FADEIN_ALPHA,
                "time", DEATH_FADETIME
            )
        );
    }

    public void ReloadScene()
    {
        StartCoroutine(LoadAsyncScene(GAME_SCENE_NAME));
    }

    public void LoadLobbyScene()
    {
        StartCoroutine(LoadAsyncScene(LOBBY_SCENE_NAME));
        SetResume();
    }
}
