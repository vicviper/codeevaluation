﻿using TMPro;

public class TitleManager : SceneAndFadeManager
{
    static public TitleManager instance;

    const string NEXT_SCENE = "LobbyScene";
    const float BLINKTIME = 3f;
    readonly iTween.LoopType BLINK_LOOP_TYPE = iTween.LoopType.pingPong;
    readonly iTween.EaseType BLINK_LOOP_EASE_TYPE = iTween.EaseType.easeOutQuad;

    public TextMeshProUGUI txtClickToStart;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        FadeoutScreenBlocker();
        BlinkStartTextEffect();
    }

    public void LoadNextScene()
    {
        LoadSceneEffect(NEXT_SCENE);
    }

    void BlinkStartTextEffect()
    {
        if (txtClickToStart == null) return;

        iTween.ColorTo(txtClickToStart.gameObject,
            iTween.Hash(
                "a", FADEOUT_ALPHA,
                "looptype", BLINK_LOOP_TYPE,
                "easetype", BLINK_LOOP_EASE_TYPE,
                "time", BLINKTIME
            )
        );
    }
}
