﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    public enum SCREEN_RESOLUTION
    {
        _1280x720 = 0,
        _1408x792 = 1,
        _1536x864 = 2
    };

    readonly List<ScreenResolution> supportedScreenResolutions = new List<ScreenResolution>()
    {
        new ScreenResolution {Width = 1280, Height = 720 },
        new ScreenResolution {Width = 1408, Height = 792 },
        new ScreenResolution {Width = 1536, Height = 864 }
    };

    public SCREEN_RESOLUTION selectScreenResolution = SCREEN_RESOLUTION._1280x720;
    public bool beFullscreen = false;
    public int reflashRate = 60;

    private int selectedIdx = 0;

    void Awake()
    {
        selectedIdx = (int)selectScreenResolution;
        Screen.SetResolution(supportedScreenResolutions[selectedIdx].Width, supportedScreenResolutions[selectedIdx].Height, beFullscreen, reflashRate);
    }
}

[System.Serializable]
public class ScreenResolution
{
    public int Width;
    public int Height;
}