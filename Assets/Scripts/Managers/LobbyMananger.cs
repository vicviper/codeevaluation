﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMananger : SceneAndFadeManager
{
    static public LobbyMananger instance;

    const string NEXT_SCENE = "GameScene";
    const float BEAM_ROTATE_ANGLE_Z = 0f;
    const float BEAM_ROTATE_TIME = 5f;
    readonly iTween.EaseType BEAM_ROTATE_EASE_TYPE = iTween.EaseType.linear;
    readonly iTween.LoopType BEAM_ROTATE_LOOP_TYPE = iTween.LoopType.pingPong;

    public Transform lightbeam;
    public BehaviourRoboLobby robo;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        FadeoutScreenBlocker();
        BeamRotateEffect();
    }

    public void LoadNextScene()
    {
        LoadSceneEffect(NEXT_SCENE);
    }

    void BeamRotateEffect()
    {
        iTween.RotateTo(lightbeam.gameObject, 
            iTween.Hash
            (
                "z", BEAM_ROTATE_ANGLE_Z,
                "time", BEAM_ROTATE_TIME,
                "easetype", BEAM_ROTATE_EASE_TYPE,
                "looptype", BEAM_ROTATE_LOOP_TYPE
            )
        );
    }
}
