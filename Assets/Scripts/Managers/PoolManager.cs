﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class PoolManager : MonoBehaviour
{
    public static PoolManager instance;
    [SerializeField]
    public List<Pool> poolList;
    public List<PoolingObject> pooling; 

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        InitPool();
    }

    void InitPool()
    {
        for (int i = 0; i < poolList.Count; i++)
        {
            for (int j = 0; j < poolList[i].minCount; j++)
            {
                LoadFromAddressable(poolList[i].addressableName, false);
            }
        }
    }

    void LoadFromAddressable(string addressName, bool actived)
    {
        Addressables.LoadAsset<GameObject>(addressName).Completed += op =>
        {
            GameObject obj = (GameObject)Instantiate(op.Result, Vector3.zero, Quaternion.identity);
            obj.SetActive(actived);
            obj.transform.parent = transform;
            if (obj.GetComponent<ParticleSystem>() != null)
            {
                obj.AddComponent<VFXDisabler>();
            }
            pooling.Add(new PoolingObject { poolName = addressName, prefab = obj});
        };
    }

    public void Spawn(string address, Vector3 position, Quaternion rotation)
    {
        PoolingObject inactiveObjects = pooling.Find(x => x.poolName == address && x.prefab.activeSelf == false);

        if (inactiveObjects != null)
        {
            inactiveObjects.prefab.SetActive(true);
            inactiveObjects.prefab.transform.position = position;
            inactiveObjects.prefab.transform.rotation = rotation;
        }
        else if (pooling.FindAll(x => x.poolName == address).Count < poolList.Find(x => x.addressableName == address).maxCount)
        {
            LoadFromAddressable(address, true);
        }
    }

    public void Despawn(GameObject obj)
    {
        string addressName = pooling.Find(x => x.prefab == obj).poolName;

        if (pooling.FindAll(x => x.prefab == obj).Count > poolList.Find(x => x.addressableName == addressName).minCount)
        {
            pooling.Remove(pooling.Find(x => x.prefab == obj));
            Destroy(obj);
        } else
        {
            PoolingObject activeObject = pooling.Find(x => x.prefab == obj);
            activeObject.prefab.SetActive(false);
        }
    }
}

[System.Serializable]
public class Pool
{
    public string addressableName;
    public int minCount;
    public int maxCount;
}

[System.Serializable]
public class PoolingObject
{
    public string poolName;
    public GameObject prefab;
}